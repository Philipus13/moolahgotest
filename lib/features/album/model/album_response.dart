
import 'dart:convert';

List<AlbumResponse> albumResponseFromJson(String str) => List<AlbumResponse>.from(json.decode(str).map((x) => AlbumResponse.fromJson(x)));

String albumResponseToJson(List<AlbumResponse> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class AlbumResponse {
    AlbumResponse({
        required this.id,
        required this.name,
        required this.role,
    });

    String id;
    String name;
    String role;

    factory AlbumResponse.fromJson(Map<String, dynamic> json) => AlbumResponse(
        id: json["id"],
        name: json["name"],
        role: json["role"],
    );

    Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "role": role,
    };
}
