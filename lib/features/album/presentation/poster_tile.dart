import 'package:philip_apps/features/album/model/album_response.dart';
import 'package:flutter/material.dart';

class PosterTile extends StatelessWidget {
  final AlbumResponse albumResponse;

  const PosterTile({Key? key, required this.albumResponse}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      color: int.parse(albumResponse.id).isEven ? Color(0xff2E3338) : Color(0xff363B3F),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                albumResponse.name,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white,fontSize: 15),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                albumResponse.role,
                overflow: TextOverflow.ellipsis,
                style: TextStyle(color: Colors.white, fontSize: 15),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
