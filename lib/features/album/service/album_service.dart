import 'package:hive/hive.dart';
import 'package:philip_apps/features/album/model/album_response.dart';
import 'package:philip_apps/core/network/base_network.dart';
import 'dart:convert';

import 'package:philip_apps/features/album/model/hive/album_model.dart';

class AlbumService extends BaseNetworking {
  Future<List<AlbumResponse>> getAlbum(
      ) async {
    final String path = 'data/data';

    final listOfAlbum = await service
        .getNoAuthToken(additionalPath: path)
        .then(
          (value) => albumResponseFromJson(json.encode(value)),
        );
    // await saveLocalAlbum(listOfAlbum);

    return listOfAlbum;
  }

  // checkHiveNull(param){

  // }

 
}
