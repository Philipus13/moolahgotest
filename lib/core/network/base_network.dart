


import 'package:philip_apps/core/config/flavor_config.dart';
import 'package:philip_apps/core/exception/remote_data_source_exception.dart';
import 'package:philip_apps/core/exception/unexpected_error_exception.dart';
import 'package:philip_apps/core/model/network_model.dart';

import 'network_utils.dart';

abstract class BaseNetworking {
  final BaseService _service = BaseService();

  BaseService get service => _service;
}

String tmdbApiKey = "8ab4ad6a324b2ab829f9c34b2c8204f0";


class BaseService {
  final String baseUrl = FlavorConfig.instance!.values.baseUrl!;

  BaseService();

  Future<Map<String, String>> _getHeaderWithAuth() async {
    //TODO: use securestorage 
    // SharedPreferences prefs = await SharedPreferences.getInstance();
    // String? token = prefs.getString('authToken');
    var header = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer token',
    };
    return header;
  }

  Map<String, dynamic> getHeaderNoAuth() {
    var header = {'Content-Type': 'application/json'};
    return header;
  }

  Future<List<dynamic>> getNoAuthToken({
    String? additionalPath,
    Map<String, dynamic>? queryParams,
  }) async {
    String path = additionalPath ?? '';
   
    try {
      final url = Uri.https(this.baseUrl, path, queryParams);

      return await NetworkUtils.get(
        url,
      ).then((response) => response );
    } catch (err) {
      throw UnexpectedErrorException(errorMessage: 'error');
    }
  }

  Future<NetworkModel> getAuthToken({
    required String additionalPath,
    Map<String, dynamic>? queryParams,
  }) async {
    try {
      final url = Uri.https(baseUrl, additionalPath, queryParams);

      return await NetworkUtils.get(url, headers: await _getHeaderWithAuth())
          .then((response) => NetworkModel.fromJson(response));
    } catch (err) {
      throw UnexpectedErrorException(errorMessage: 'error');
    }
  }

  Future<String> postGetToken ({String? username, String? password}) async {
    final String loginPath = 'api/v1/auth/token';
    try {
      final url = Uri.https(baseUrl, loginPath);
      final body = <String, String?>{
        'username': username,
        'password': password,
        'grant_type': 'password',
      };

      return await NetworkUtils.post(url, body: body)
          .then((response) => response['access_token']);
    } catch (err) {
      if (err is RemoteDataSourceException) {
        throw UnexpectedErrorException(errorMessage: err.message);
      } else {
        throw UnexpectedErrorException(errorMessage: 'error');
      }
    }
  }

   Future<NetworkModel> postNoAuthToken({
    Map<String, String?>? data,
    required String path,
  }) async {
    print('Method POST : ${this.baseUrl}$path \n Body: $data');
    try {
      final url = Uri.https(baseUrl, path);

      return await NetworkUtils.post(url,
              headers: await _getHeaderWithAuth(), body: data)
          .then((response) => NetworkModel.fromJson(response));
    } catch (err) {
      if (err is RemoteDataSourceException) {
        throw UnexpectedErrorException(errorMessage: err.message);
      } else {
        throw UnexpectedErrorException(errorMessage: 'error');
      }
      // return NetworkModel.fromJson(err.response.data);
    }
  }

}
