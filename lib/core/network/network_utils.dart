import 'dart:convert';

import 'package:philip_apps/core/exception/remote_data_source_exception.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

class NetworkUtils {
  static Future _helperFile(
    List<MultipartFile> multiPartFileList,
    String method,
    Uri url, {
    Map<String, String>? headers,
    Map<String, String>? body,
  }) async {
    late http.MultipartRequest request;

    //if set in this dont pass null variable, cause it makes unhandled exception
    try {
     
        request = http.MultipartRequest(method, url)
          ..headers.addAll(headers!)
          ..fields.addAll(body!)
          ..files.addAll(multiPartFileList);
     
    } catch (e) {
      e.toString();
    }

    final streamedReponse = await request.send();
    final statusCode = streamedReponse.statusCode;
    final decoded = json.decode(await streamedReponse.stream.bytesToString());

    if (statusCode == 401) {}

    if (statusCode < 200 || statusCode >= 300) {
      throw RemoteDataSourceException(statusCode, decoded['message'],
          decoded['field_errors'], decoded['success']);
    }

    return decoded;
  }

  static Future _helper(
    String method,
    Uri url, {
    Map<String, String>? headers,
    Map<String, String?>? body,
  }) async {
    String bodys = jsonEncode(body);
    late http.Response response;

    switch (method) {
      case 'POST':
        response = await http.post(url, body: bodys, headers: headers);

        break;
      case 'PUT':
        response = await http.put(url, body: bodys, headers: headers);

        break;
      case 'PATCH':
        response = await http.patch(url, body: bodys, headers: headers);

        break;
      default:
    }

    final statusCode = response.statusCode;
    dynamic decoded;

    if (statusCode < 200 || statusCode >= 300) {
      try {
        decoded = json.decode(response.body);
        throw RemoteDataSourceException(statusCode, decoded['message'],
            decoded['field_errors'], decoded['success']);
      } catch (e) {
        if (e is RemoteDataSourceException) {
          throw RemoteDataSourceException(statusCode, decoded['message'],
              decoded['field_errors'], decoded['success']);
        }
        if (e is FormatException) {
          throw RemoteDataSourceException(
              statusCode, 'Invalid error', null, false);
        }
      }
    }
    decoded = json.decode(response.body);

    return decoded;
  }

  static Future get(
    Uri url, {
    Map<String, String>? headers,
  }) async {
    final response = await http.get(url, headers: headers);

    // final body = response.body;
    final statusCode = response.statusCode;
    dynamic decoded;

    
    if (statusCode < 200 || statusCode >= 300) {
      try {
        decoded = json.decode(response.body);
      } catch (e) {
        if (e is RemoteDataSourceException) {
          throw RemoteDataSourceException(statusCode, decoded['message'],
              decoded['field_errors'], decoded['success']);
        }
        if (e is FormatException) {
          throw RemoteDataSourceException(
              statusCode, 'Invalid error', null, false);
        }
      }
      throw RemoteDataSourceException(statusCode, decoded['message'],
          decoded['field_errors'], decoded['success']);
    }
    decoded = json.decode(response.body);

    return decoded;
  }

  static Future<int> getStatusCode(
    Uri url, {
    Map<String, String>? headers,
  }) async {
    final response = await http.get(url, headers: headers);
    final statusCode = response.statusCode;
    return statusCode;
  }

  static Future post(
    Uri url, {
    Map<String, String>? headers,
    Map<String, String?>? body,
  }) =>
      _helper(
        'POST',
        url,
        headers: headers,
        body: body,
      );

  static Future put(
    Uri url, {
    Map<String, String>? headers,
    body,
  }) =>
      _helper(
        'PUT',
        url,
        headers: headers,
        body: body,
      );

  static Future patch(
    Uri url, {
    Map<String, String>? headers,
    Map<String, String>? body,
  }) =>
      _helper(
        'PATCH',
        url,
        headers: headers,
        body: body,
      );
  static Future uploadImage(
    List<MultipartFile> multiPartFileList,
    String method,
    Uri url, {
    Map<String, String>? headers,
    Map<String, String>? body,
  }) =>
      _helperFile(
        multiPartFileList,
        method,
        url,
        headers: headers,
        body: body,
      );
}
