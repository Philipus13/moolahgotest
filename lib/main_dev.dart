import 'package:philip_apps/app.dart';
import 'package:philip_apps/core/config/flavor_config.dart';
import 'package:philip_apps/core/style/base_style.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:hive/hive.dart';

import 'core/bloc_delegate/simple_bloc_delegate.dart';
import 'features/album/model/hive/album_model.dart';

Future<void> main() async {
  FlavorConfig(flavor: Flavor.DEV,
     
      values: FlavorValues(baseUrl: "2a73a408-d6fc-42c1-acc4-4c11ce5661a3.mock.pstmn.io"));
  
  Bloc.observer = SimpleBlocDelegate();
  Hive
  ..initFlutter()
  ..registerAdapter(AlbumHiveModelAdapter());

   
  runApp(MainApplication());
}

class MainApplication extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: BaseStyle.darkBlue,
        primaryColorLight: BaseStyle.lightBlue,
      ),
      initialRoute: '/',
      routes: {
        '/': (context) => App(),
      },
    );
  }
}