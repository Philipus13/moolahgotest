import 'package:philip_apps/features/album/presentation/album_page.dart';
import 'package:philip_apps/core/widget/home_navigation_builder.dart';
import 'package:flutter/material.dart';

class App extends StatefulWidget {
  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  @override
  void initState() {
    super.initState();
  }

 

  @override
  Widget build(BuildContext context) {
    return 
      HomeNavigationBuilder(
        builder: (context, tabItem) {
            return Padding(
              padding: const EdgeInsets.all(20.0),
              child: AlbumPage(mode: 0),
            );
          
        },
      );
  }
}
